package com.obdtrackapp.dalton.obdiitrackapp.obd2_peek;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.obdtrackapp.dalton.obdiitrackapp.obd2_peek.R;

public class Step2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step2);
    }


    public void backClicked(View view) {
        finish();
    }
}
